const mongoose =require('mongoose');

var Employee = mongoose.model('Employee',{
    id:{type: String},
    name:{type:String},
    department:{type:String}
});

module.exports={Employee};