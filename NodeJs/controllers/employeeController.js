const express =require('express');
var router=express.Router();
var Objectid=require('mongoose').Types.ObjectId;

var  {Employee}=require('../models/employee');
//=>localhost/3000/employee
    router.get('/',(req,res)=>{
        Employee.find((err,docs)=>{
            if(!err){res.send(docs);}
            else{
                console.log('Error In Retrieving Employee : '+ JSON.stringify(stringify(err,undefined,2)));
            }
        });
});
//findByIdByMongoose
/*router.get('/:id',(req,res)=>{
    if(!Objectid.isValid(req.params.id))
    return res.status(400).send(`No Record with given ID : ${req.params.id}`);
    Employee.findById(req.params.id,(err,doc)=>{
        if(!err){res.send(doc);}
        else{console.log('Error In Retrieving Employees :'+JSON.stringify(err,undefined,2));}
    });
   
});*/
//findbyCustomID
router.get('/:id',(req,res)=>{
      Employee.findOne(req.params,(err,doc)=>{
        if(!err){res.send(doc);}
        else{console.log('Error In Retrieving Employees :'+JSON.stringify(err,undefined,2));}
    });
   
});

router.post('/',(req,res)=>{
    var emp=new Employee(
        {
            id:req.body.id,
            name:req.body.name,
            department:req.body.department,
        });
        emp.save((err,doc)=>{
            if(!err){res.send(doc);}
            else{console.log('Error in Employee Save :'+JSON.stringify(err,undefined,2));}
        });
});

router.put('/:id',(req,res)=>{
    if(!Objectid.isValid(req.params.id))
        return res.status(400).send(`No Record with given ID : ${req.params.id}`);
    var emp={
        id:req.body.id,
        name:req.body.name,
        department:req.body.department,
    };
    Employee.findByIdAndUpdate(req.params.id,{$set:emp},{new:true},(err,doc)=>{
        if(!err){res.send(doc);}
        else{console.log('Error in Employee Update :'+JSON.stringify(err,undefined,2));}
    });
});

router.delete('/:id',(req,res)=>{
    if(!Objectid.isValid(req.params.id))
        return res.status(400).send(`No Record with given ID : ${req.params.id}`);
    Employee.findByIdAndRemove(req.params.id,(err,doc)=>{
        if(!err){res.send(doc);}
        else{console.log('Error in Employee Delete :'+JSON.stringify(err,undefined,2));}
    });
});
module.exports=router;